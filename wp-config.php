<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'baitapba_db');

/** MySQL database username */
define('DB_USER', 'baitapba_db');

/** MySQL database password */
define('DB_PASSWORD', 'pInGAnAPBp');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$R~uwTH+@%MkI-+8G9&|6-B~=0U*1|v-V-X: fHnUo6+Gnr&@Ot2(8H2Ug}27fC ');
define('SECURE_AUTH_KEY',  ',rt1m>T}V=uyOJza|U+kp50.-ao!jPb9&2J=pc$W|DVu:%D)>)qJ{+f<njlH:Dad');
define('LOGGED_IN_KEY',    '9`^J.m)Rvmc*^.OP80?!!wjWvcT0S[>,9~=|B^H^1s;yn}$iC`-[7p>n7j?d=R}&');
define('NONCE_KEY',        '(g)]c1g<%yyXBHr2eC@~ ;4A3co+SCYzOId GkMTB8[aYgQFv+OtlrEQ%f+k|gG&');
define('AUTH_SALT',        '<d6v223!iFo[^|)sl/`m-[IgCU~E+0b#6Th=pbR`HxH_CbezVys344%INtU3pu:)');
define('SECURE_AUTH_SALT', '-|t/t)t#gF-M:.ZXC)>W*lKq6PuFNpvr$3ST?1T_c(e$R1G?9N#7h<k0cUF%|OJB');
define('LOGGED_IN_SALT',   'Gu@NLtbD)V6k$qrS!#I4wIf40todl*$DJO&j*_75!0E0K;Hgg)H/+,$rQfJitqP#');
define('NONCE_SALT',       '07[1/#Bbt#<t&ClIAe]8z!:Y?AYfy&&`!O)8gAOPE$nN]$tpCN%vp-Xc$0B`wynl');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
